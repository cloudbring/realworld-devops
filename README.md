# realworld-gitlab

An exploration of deploying various Realworld.io (Conduit Medium Clone Backend + Frontend) apps using gitlab vs other technologies.

Focused on the DevOps features and complexity of actually deploying the mother of all demos with various tools.